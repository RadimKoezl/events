from django.urls import path

from . import views

app_name = 'events'

urlpatterns = [
    path('new/', views.CreateEventView.as_view(), name='create_event'),
    path('mine/', views.MyEventsView.as_view(), name='my_events'),
    path('search/', views.EventSearchView.as_view(), name='search'),

    path('detail/<slug:slug>/', views.EventDetailView.as_view(), name='detail'),
    path('update/<slug:slug>/', views.UpdateEventView.as_view(), name='update_event'),
    path('delete/<slug:slug>/', views.DeleteEventView.as_view(), name='delete_event'),

    path('<slug:event_slug>/update-run/<int:event_run_id>', views.UpdateEventRunView.as_view(), name='update_event_run'),
    path('<slug:event_slug>/delete-run/<int:event_run_id>/', views.DeleteEventRunView.as_view(), name='delete_event_run'),
    path('<slug:event_slug>/new-run/', views.CreateEventRunView.as_view(), name='create_event_run'),

    path('<category>/', views.EventListView.as_view(), name='events'),
]